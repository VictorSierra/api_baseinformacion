'use strict'
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
//carga rutas
var user_routes = require('./routes/user');

app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
app.use(bodyParser.json({ limit: '5mb', extended: false }));


//configuracion de cabeceras http   
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'authorization,X-API-KEY,Origin,X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET,POST');
    res.header('Allow', 'GET', 'POST');
    res.header()
    next();
});
//rutas base
app.use('/api', user_routes);

module.exports = app;