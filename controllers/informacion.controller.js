'use strict'

let Global = require('../global');
let redis = require('redis');

const canal = 'nuevapublicacion';

function aregarInformacion(req, res) {

    if (req.body.data != undefined && req.body.dataName != undefined) {

        console.log("--------------------------------------------------------------------");
        console.log("---- Archivo de Información: " + req.body.dataName);
        console.log("--------------------------------------------------------------------");

        switch (req.body.dataName) {
            // Descarga
            case 'descarga.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("descarga", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });

                break;




            // Diputaciones
            case 'DIP_DET_ENTIDAD.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("DIP_DET_ENTIDAD", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'DIP_DISTRITO.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("DIP_DISTRITO", req.body.data);
                redisClient.publish(canal, req.body.dataName);
                });
                
                break;

            case 'DIP_ENTIDAD.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("DIP_ENTIDAD", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'DIP_SECCION.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("DIP_SECCION", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;




            //Gubernatura
            case 'GOB_DET_ENTIDAD.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("GOB_DET_ENTIDAD", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'GOB_DISTRITO.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("GOB_DISTRITO", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'GOB_ENTIDAD.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("GOB_ENTIDAD", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'GOB_SECCION.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("GOB_SECCION", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;




            //Municipio
            case 'MUN_DETALLE.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("MUN_DETALLE", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'MUN_ENTIDAD.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("MUN_ENTIDAD", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'MUN_SECCION.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("MUN_SECCION", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;

            case 'MUNICIPIOS.json':
                console.log("Archivo recpcionado " + req.body.dataName);
                Global.Global.urlredis.forEach(function(server){
                    var redisClient = redis.createClient(server);
                    redisClient.set("MUN_MUNICIPIOS", req.body.data);
                    redisClient.publish(canal, req.body.dataName);
                });
                break;
            //default
            default:
                console.log("error: ¿Qué rayos mandaste? - Verificar Tamano del Archivo");
                res.status(400).send({ message: 'Información recepcionada Desconocida' });
                break;
        }
        res.status(200).send({ message: 'Dato entregado'});

    } else {
        console.log("error: ¿Qué rayos mandaste?");
        res.status(400).send({ message: 'Información recepcionada Desconocida' });
    }
}


module.exports = {
    aregarInformacion
}