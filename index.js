'use strict'
var app = require('./app');

var Global = require('./global');
let redis= require('redis');

const canal='nuevapublicacion';

var port = process.env.PORT || 3902;
app.listen(port, function () {
    console.log('Servidor de api rest escuchando en el puerto: ' + port);    
});

Global.Global.urlredis.forEach(function(server){

    let redisClient=redis.createClient(server);    
    redisClient.subscribe(canal);
    
    console.log("Server Redis: "+server);
    redisClient.get("descarga",function (err, reply) {
        console.log("Valor prueba: "+reply);
    }); 

});